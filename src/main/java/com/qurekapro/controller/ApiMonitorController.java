package com.qurekapro.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qurekapro.service.ApiMonitorService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class ApiMonitorController {
	
	@Autowired
	ApiMonitorService apiService;
	
	@PostMapping(value = "monitor-api")
	public Map<String, Object> monitorApi() {
		//log.info("starting API monitor process");
		return apiService.CheckAllApis();
	}
}
