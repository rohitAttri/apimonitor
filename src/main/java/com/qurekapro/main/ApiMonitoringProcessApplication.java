package com.qurekapro.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.qurekapro")
public class ApiMonitoringProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMonitoringProcessApplication.class, args);
	}

}
