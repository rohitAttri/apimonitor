package com.qurekapro.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.qurekapro.config.PropertiesConfig;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ApiMonitorService {

	@Autowired
	PropertiesConfig propConfig;
	
//	@Autowired
//	RestTemplate restTemplate;

	public Map<String, Object> CheckAllApis() {
		Map<String, Object> response = new HashMap<>();
		response.put("status", 0);
		try {
			String apiList = propConfig.getApiNameList();
			if(apiList != "" || apiList != null) {
				//log.info("reading apis from property file ...);
				JSONObject jsonObject = new JSONObject(apiList);
				Map<String, String> apis = new Gson().fromJson(jsonObject.toString(), HashMap.class);
				if(!apis.isEmpty() && apis.size()>0 && apis != null){
					System.out.println(apis.toString());
					//log.info("reading successful);
					for (Map.Entry<String,String> api : apis.entrySet()) {
						System.out.println("Key = " + api.getKey() + ", Value = " + api.getValue()); 
					}  
				}else {
					//log.info("something went wrong !!");
				}
				
			}else {
				
			}
		} catch (Exception e) {
			//log.error("Exception in ApiMonitorService CheckAllApis() : {}", e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
}
