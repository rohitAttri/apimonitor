package com.qurekapro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
@Setter
@Getter
@ToString
public class PropertiesConfig {

	@Value("${api.name.list}")
	private String apiNameList;
	
	@Value("${base.url}")
	private String baseUrl;

	public String getApiNameList() {
		return apiNameList;
	}

	public void setApiNameList(String apiNameList) {
		this.apiNameList = apiNameList;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
}
